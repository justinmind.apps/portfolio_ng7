import { credentials } from '.env/environment';

export const environment = {
  production: true,
  hmr: false,
  ngxsDebuggerDisabled: true,
  enableTracing: false,
  firebase: credentials.firebase
};
