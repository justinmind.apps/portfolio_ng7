import { credentials } from '../../.env/environment';
import 'zone.js/dist/zone-error';

export const environment = {
  production: false,
  hmr: false,
  sentryDNS: credentials.sentryDNS,
  ngxsDebuggerDisabled: false,
  enableTracing: true,
  firebase: credentials.firebase
};
