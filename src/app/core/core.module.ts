import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreState } from './core.state';
import { environment } from 'src/environments/environment';
import { LayoutState } from './layout/layout.state';
import { NgModule } from '@angular/core';
import { NgxsEmitPluginModule } from '@ngxs-labs/emitter';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { SplashscreenComponent } from '../components/splashscreen/splashscreen.component';
import { ToolbarDirective } from '../components/toolbar/toolbar.directive';
@NgModule({
  imports: [
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    BrowserAnimationsModule,
    NgxsModule.forRoot([CoreState, LayoutState], {
      developmentMode: !environment.production
    }),
    NgxsEmitPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({
      key: ['PORTFOLIO']
    }),
    NgxsReduxDevtoolsPluginModule.forRoot()
  ],
  exports: [SplashscreenComponent],
  declarations: [SplashscreenComponent],
  providers: []
})
export class CoreModule {}
