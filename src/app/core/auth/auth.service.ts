import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import { ErrorService } from '../error-handling/error-handler.service';
import { first } from 'rxjs/operators';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private _afAuth: AngularFireAuth,
    private errorService: ErrorService
  ) {}

  getUser = (): Promise<firebase.User> =>
    this._afAuth.authState.pipe<firebase.User>(first()).toPromise()

  private async oAuthLogin(provider): Promise<auth.UserCredential> {
    return await this._afAuth.auth.signInWithPopup(provider);
  }

  private async login(provider): Promise<auth.UserCredential | null> {
    try {
      return await this.oAuthLogin(provider);
    } catch (error) {
      this.errorService.createMessage(error);
      return;
    }
  }

  async googleLogin(): Promise<boolean> {
    const isLoggedIn = !!(await this.login(new auth.GoogleAuthProvider()));

    return isLoggedIn;
  }

  async signOut() {
    await this._afAuth.auth.signOut();
  }
}
