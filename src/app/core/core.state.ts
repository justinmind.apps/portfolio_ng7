import { CoreStateModel } from './core.model';
import { LayoutState } from './layout/layout.state';
import { State } from '@ngxs/store';

@State<CoreStateModel>({
  name: 'PORTFOLIO',
  defaults: {
    user: undefined
  },
  children: [LayoutState]
})
export class CoreState {}
