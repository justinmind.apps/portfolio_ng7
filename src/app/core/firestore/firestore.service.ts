import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { Injectable } from '@angular/core';
import { map, take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

type CollectionPredicate<T> = string | AngularFirestoreCollection<T>;
type DocPredicate<T> = string | AngularFirestoreDocument<T>;

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  constructor(public afs: AngularFirestore) {}

  /// **************
  /// Get a Reference
  /// **************

  col<T>(ref: CollectionPredicate<T>, queryFn?): AngularFirestoreCollection<T> {
    return typeof ref === 'string' ? this.afs.collection<T>(ref, queryFn) : ref;
  }

  doc<T>(ref: DocPredicate<T>): AngularFirestoreDocument<T> {
    return typeof ref === 'string' ? this.afs.doc<T>(ref) : ref;
  }

  /// **************
  /// Get Data
  /// **************

  doc$<T>(ref: DocPredicate<T>): Observable<T> {
    return this.doc(ref)
      .snapshotChanges()
      .pipe(
        map(doc => {
          return doc.payload.data() as T;
        })
      );
  }

  col$<T>(ref: CollectionPredicate<T>, queryFn?): Observable<T[]> {
    return this.col(ref, queryFn)
      .snapshotChanges()
      .pipe(
        map(docs => {
          return docs.map(a => a.payload.doc.data()) as T[];
        })
      );
  }

  /// with Ids
  colWithIds$<T>(
    ref: CollectionPredicate<T>,
    queryFn?
  ): Observable<{ id: string; data: T }[]> {
    return this.col(ref, queryFn)
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;

            return { id, data };
          });
        })
      );
  }

  /// **************
  /// Write Data
  /// **************

  /// Firebase Server Timestamp
  get timestamp() {
    return firestore.FieldValue.serverTimestamp();
  }

  set<T>(ref: DocPredicate<T>, data: any) {
    const timestamp = this.timestamp;
    return this.doc(ref)
      .set({
        ...data,
        updatedAt: timestamp,
        createdAt: timestamp
      })
      .then(setData => {
        // return false for use didn't exist before
        return false;
      })
      .catch(errorData => {
        return false;
      });
  }

  update<T>(ref: DocPredicate<T>, data: any) {
    return this.doc(ref).update({
      ...data,
      updatedAt: this.timestamp
    });
  }

  // async createOrIgnoreUser(credential: auth.UserCredential) {
  //   const uid = credential.user.uid;
  //   const email = credential.user.email;
  //   const docPredicate = `users/${uid}`;

  //   const userExists = await this.checkIfDocExists(docPredicate);

  //   if (userExists) {
  //     const user = startUser(uid, email);
  //     this.update(docPredicate, user);
  //   } else {
  //     const user = startUser(uid, email, true);

  //       await this.set(docPredicate, user);

  //       const minimis: Minimi[] = startMinimi(uid);
  //       return await this.gameService.addMinimi(user.uid, minimis);
  //     }
  //     return user;
  // }

  delete<T>(ref: DocPredicate<T>) {
    return this.doc(ref).delete();
  }

  add<T>(ref: CollectionPredicate<T>, data) {
    const timestamp = this.timestamp;
    return this.col(ref).add({
      ...data,
      updatedAt: timestamp,
      createdAt: timestamp
    });
  }

  geopoint(lat: number, lng: number) {
    return new firestore.GeoPoint(lat, lng);
  }

  /// If doc exists update, otherwise set
  async upsert<T>(ref: DocPredicate<T>, data: any) {
    const doc = this.doc(ref)
      .snapshotChanges()
      .pipe(take(1))
      .toPromise();

    const snap = await doc;
    return snap.payload.exists ? this.update(ref, data) : this.set(ref, data);
  }

  async checkIfDocExists<T>(ref: DocPredicate<T>): Promise<boolean> {
    const doc = this.doc(ref)
      .snapshotChanges()
      .pipe(take(1))
      .toPromise();

    const snap = await doc;
    return snap.payload.exists;
  }

  /// **************
  /// Inspect Data
  /// **************

  inspectDoc(ref: DocPredicate<any>): void {
    const tick = new Date().getTime();
    this.doc(ref)
      .snapshotChanges()
      .pipe(
        take(1),
        tap(d => {
          const tock = new Date().getTime() - tick;
          console.log(`Loaded Document in ${tock}ms`, d);
        })
      )
      .subscribe();
  }

  inspectCol(ref: CollectionPredicate<any>): void {
    const tick = new Date().getTime();
    this.col(ref)
      .snapshotChanges()
      .pipe(
        take(1),
        tap(c => {
          const tock = new Date().getTime() - tick;
          console.log(`Loaded Collection in ${tock}ms`, c);
        })
      )
      .subscribe();
  }

  /// **************
  /// Create and read doc references
  /// **************

  /// create a reference between two documents
  connect(host: DocPredicate<any>, key: string, doc: DocPredicate<any>) {
    return this.doc(host).update({ [key]: this.doc(doc).ref });
  }

  /// returns a documents references mapped to AngularFirestoreDocument
  docWithRefs$<T>(ref: DocPredicate<T>) {
    return this.doc$(ref).pipe(
      map(doc => {
        for (const k of Object.keys(doc)) {
          if (doc[k] instanceof firestore.DocumentReference) {
            doc[k] = this.doc(doc[k].path);
          }
        }
        return doc;
      })
    );
  }

  /// **************
  /// Atomic batch example
  /// **************

  // initializeUserForGalaxy(_uid) {
  //   const universeId = '7pAP4IDihpIJehtERxz1';
  //   const batch = firestore().batch();
  //   const currentTime = this.timestamp;

  //   // Ressources
  //   const ressources = new Ressources();
  //   const ressUser = {
  //     ...ressources,
  //     universeId,
  //     timestamp: currentTime
  //   };

  //   const ressourcesDoc = firestore().doc(`ressources/${_uid}`);

  //   const buildingsDoc = firestore().doc(`buildings/${_uid}`);

  //   batch.update(ressourcesDoc, ressUser);

  //   return batch.commit();
  // }

  /// Just an example, you will need to customize this method.
  atomic() {
    const batch = firestore().batch();
    /// add your operations here

    const itemDoc = firestore().doc('items/myCoolItem');
    const userDoc = firestore().doc('users/userId');

    const currentTime = this.timestamp;

    batch.update(itemDoc, { timestamp: currentTime });
    batch.update(userDoc, { timestamp: currentTime });

    /// commit operations
    return batch.commit();
  }
}
