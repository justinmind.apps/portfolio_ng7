export class SetMediaQuery {
  public static readonly type = `[Layout] SetMediaQuery`;
  constructor(public payload: boolean) {}
}

export class OpenSidenav {
  public static readonly type = `[Layout] Open Sidenav`;
  constructor(public payload: boolean) {}
}
