import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { EmitterAction, Receiver } from '@ngxs-labs/emitter';
import { LayoutStateModel } from './layout.model';
import { map, take, tap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { OpenSidenav, SetMediaQuery } from './layout.actions';
import {
  Selector,
  State,
  StateContext,
  Store
  } from '@ngxs/store';

export class StartLoading {
  public static readonly type = `[Layout] StartLoading`;
}

@State<LayoutStateModel>({
  name: 'Layout',
  defaults: {
    isLoading: true,
    isFullscreen: false,
    sidenavIsVisible: false,
    toolbarIsVisible: true
  }
})
export class LayoutState {
  /**
   * Gets app-loading-state
   * @returns true if is loading; else false
   */
  @Selector()
  static isLoading(state: LayoutStateModel): boolean {
    return state.isLoading;
  }

  /**
   * Starts app-loading-status
   * @var isLoading = true
   */
  @Receiver({ action: StartLoading })
  static startLoading({
    getState,
    patchState
  }: StateContext<LayoutStateModel>) {
    patchState({
      ...getState(),
      isLoading: true
    });
  }

  /**
   * Stops app-loading-status
   */
  @Receiver()
  static stopLoading({ getState, patchState }: StateContext<LayoutStateModel>) {
    patchState({
      ...getState(),
      isLoading: false
    });
  }

  /**
   * Gets screen-size-state
   * @returns true if 1024px and up; else false
   */
  @Selector()
  static isMediumUp(state: LayoutStateModel): boolean {
    return state.isMediumUp;
  }

  /**
   * Gets fullscreen-state
   * @returns isFullscreen = true; else false
   */
  @Selector()
  static isFullscreen(state: LayoutStateModel): boolean {
    return state.isFullscreen;
  }

  /**
   * Toggles fullscreen-state
   * @param isFullscreen: boolean
   */
  @Receiver()
  static toggleFullscreen({
    getState,
    patchState
  }: StateContext<LayoutStateModel>) {
    const state = getState();

    patchState({
      ...state,
      isFullscreen: !state.isFullscreen
    });
  }

  /**
   * Gets sidenav visibility-state
   * @param sidenavIsVisible: open = true; else false
   */
  @Selector()
  static sidenavIsVisible(state: LayoutStateModel): boolean {
    return state.sidenavIsVisible;
  }

  /**
   * Toggles sidenav-state
   */
  @Receiver()
  static toggleSidenav({
    getState,
    patchState
  }: StateContext<LayoutStateModel>) {
    const state = getState();
    console.warn('Sidenav state: ', state.sidenavIsVisible);
    patchState({
      ...state,
      sidenavIsVisible: !state.sidenavIsVisible
    });
  }

  /**
   * Opens sidenav if payload is true
   */
  @Receiver()
  static openSidenav(
    { getState, patchState }: StateContext<LayoutStateModel>,
    { payload }: EmitterAction<boolean>
  ) {
    const state = getState();
    patchState({
      ...state,
      sidenavIsVisible: payload
    });
  }

  /**
   *  Set toolbar visibility-state
   */
  @Receiver()
  static showToolbar(
    { getState, patchState }: StateContext<LayoutStateModel>,
    { payload }: EmitterAction<boolean>
  ) {
    const state = getState();
    patchState({
      ...state,
      toolbarIsVisible: payload
    });
  }

  /**
   *  Get toolbar visibility-state
   */
  @Selector()
  static toolbarIsVisible(state: LayoutStateModel): boolean {
    return state.toolbarIsVisible;
  }

  constructor(
    private store: Store,
    public breakpointObserver: BreakpointObserver
  ) {}

  ngxsOninit() {
    this.store.dispatch(new SetMediaQuery(true));
  }
}
