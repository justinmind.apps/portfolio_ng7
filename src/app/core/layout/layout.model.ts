export interface LayoutStateModel {
  isLoading: boolean;
  sidenavIsVisible: boolean;
  isFullscreen: boolean;
  toolbarIsVisible: boolean;
  isMediumUp?: boolean;
}
