import { auth } from 'firebase';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  constructor() {}

  createMessage(error: auth.Error) {
    console.warn('⚠ ERROR', error);
    switch (error.code) {
      case 'auth/user-disabled':
        console.warn('Der Nutzeraccount wurde deaktiviert.');
        break;
      case 'auth/operation-not-allowed':
        console.warn('Der Provider existiert nicht');
        break;
      case 'not-found':
        console.warn(
          'Document zum Aktualisieren konnte nicht gefunden werden.'
        );
        break;
      default:
        console.warn('Es ist ein Fehler mit der Datenbank aufgetreten.');
        break;
    }
  }
}
