import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShellGuard } from './pages/shell/shell.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: './pages/shell/shell.module#ShellModule',
    canActivate: [ShellGuard]
  },
  {
    path: 'signin',
    loadChildren: './pages/signin/signin.module#SigninModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
