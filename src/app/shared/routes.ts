export namespace AppRoutes {
  export function home(): string[] {
    return ['/'];
  }

  export function overview(): string[] {
    return ['/overview'];
  }

  export function skills(): string[] {
    return ['/skills'];
  }

  export function milestones(): string[] {
    return ['/milestones'];
  }
  export function aboutPortfolio(): string[] {
    return ['/about-portfolio'];
  }
  export function sources(): string[] {
    return ['/sources'];
  }
  export function impressum(): string[] {
    return ['/impressum'];
  }
}
