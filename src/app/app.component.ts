import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit
  } from '@angular/core';
import { Emittable, Emitter } from '@ngxs-labs/emitter';
import { first, tap } from 'rxjs/operators';
import { LayoutState } from './core/layout/layout.state';
import { Observable, Subscription } from 'rxjs';
import { Select } from '@ngxs/store';
import { splashAnimations } from './components/splashscreen/splashscreen.animations';

@Component({
  selector: 'root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [splashAnimations]
})
export class AppComponent implements OnInit, AfterViewInit {
  // *****************************
  // NGXS Store
  // *****************************
  @Select(LayoutState.isLoading) isLoading$: Observable<boolean>;

  @Emitter(LayoutState.startLoading) startLoading: Emittable<void>;

  // *****************************
  // Custom Properties
  // *****************************
  isLoadingSub: Subscription;

  // *****************************
  // Hooks
  // *****************************
  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.startLoading.emit();
  }

  ngAfterViewInit() {
    this.isLoading$
      .pipe(
        first(isLoading => !isLoading),
        tap(() => this.cd.detectChanges())
      )
      .subscribe();
  }
}
