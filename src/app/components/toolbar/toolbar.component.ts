import {
  AfterViewInit,
  Component,
  HostBinding,
  Inject,
  OnInit
  } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger
  } from '@angular/animations';
import {
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  share,
  throttleTime
  } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { Emittable, Emitter } from '@ngxs-labs/emitter';
import { LayoutState } from 'src/app/core/layout/layout.state';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { VisibilityState } from 'src/app/shared/models/visibility-state.model';

@Component({
  selector: 'header',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  animations: [
    trigger('toggle', [
      state(
        VisibilityState.Hidden,
        style({ opacity: 0, transform: 'translateY(-100%)' })
      ),
      state(
        VisibilityState.Visible,
        style({ opacity: 1, transform: 'translateY(0)' })
      ),
      transition('* => *', animate('200ms ease-in'))
    ])
  ]
})
export class ToolbarComponent {
  @Emitter(LayoutState.toggleSidenav)
  public toggleSidenav: Emittable<null>;

  @Select(LayoutState.isFullscreen) isFullscreen$: Observable<boolean>;

  @Emitter(LayoutState.toggleFullscreen)
  public toggleFullscreen: Emittable<void>;

  currentTheme = 'blue';

  constructor(@Inject(DOCUMENT) private document: Document) {}

  toggleTheme() {
    if (this.currentTheme === 'blue') {
      this.document.body.classList.remove('portfolio-light');
      this.document.body.classList.add('portfolio-dark');
      this.currentTheme = 'red';
    } else if (this.currentTheme === 'red') {
      this.document.body.classList.remove('portfolio-dark');
      this.document.body.classList.add('portfolio-light');
      this.currentTheme = 'blue';
    } else {
      this.document.body.classList.remove('portfolio-light');
      this.document.body.classList.add('portfolio-dark');
      this.currentTheme = 'blue';
    }
  }
}
