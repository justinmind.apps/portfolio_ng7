import { Direction } from 'src/app/shared/models/direction.model';
import {
  Directive,
  ElementRef,
  HostBinding,
  OnInit
  } from '@angular/core';
import {
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  share,
  throttleTime
  } from 'rxjs/operators';
import { fromEvent, Observable } from 'rxjs';
import { LayoutState } from 'src/app/core/layout/layout.state';
import { Select } from '@ngxs/store';
import { VisibilityState } from 'src/app/shared/models/visibility-state.model';

@Directive({
  selector: '[bkToggle]'
})
export class ToolbarDirective implements OnInit {
  private isVisible = true;

  @Select(LayoutState.toolbarIsVisible) toolbarIsVisible$: Observable<boolean>;

  @HostBinding('@toggle')
  get toggle(): VisibilityState {
    return this.isVisible ? VisibilityState.Visible : VisibilityState.Hidden;
  }

  ngOnInit() {
    this.toolbarIsVisible$.subscribe(isVisible => {
      console.warn('isVisible: ', isVisible);
      this.isVisible = isVisible;
    });
  }
}
