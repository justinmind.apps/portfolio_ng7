import {
  animate,
  style,
  transition,
  trigger
  } from '@angular/animations';

export const splashAnimations = [
  trigger('loading', [
    transition(':leave', [
      style({ opacity: 2 }),
      animate('1s', style({ opacity: 0 }))
    ])
  ])
];
