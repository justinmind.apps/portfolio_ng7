export interface SidenavItem {
  ref: string[];
  text: string;
  appendLine: boolean;
  icon: string;
}
