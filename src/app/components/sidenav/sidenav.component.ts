import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding
  } from '@angular/core';
import { Emittable, Emitter } from '@ngxs-labs/emitter';
import { LayoutState } from 'src/app/core/layout/layout.state';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { sidenavItems } from './sidenav-items';

@Component({
  selector: 'nav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  animations: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent implements AfterViewInit {
  links = [];

  constructor(private cd: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.links = sidenavItems;

    this.cd.detectChanges();
  }
}
