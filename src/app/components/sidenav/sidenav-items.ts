import { AppRoutes } from 'src/app/shared/routes';
import { SidenavItem } from './sidenav-item.model';

export const sidenavItems: SidenavItem[] = [
  {
    ref: AppRoutes.overview(),
    text: 'Dashboard',
    appendLine: false,
    icon: 'fa-home'
  },
  {
    ref: AppRoutes.skills(),
    text: 'Skills',
    appendLine: false,
    icon: 'fa-link'
  },
  {
    ref: AppRoutes.milestones(),
    text: 'Curriculum Vitae',
    appendLine: false,
    icon: 'fa-link'
  },
  {
    ref: AppRoutes.sources(),
    text: 'Sources',
    appendLine: true,
    icon: 'fa-file-archive'
  },
  {
    ref: AppRoutes.aboutPortfolio(),
    text: 'App Info',
    appendLine: false,
    icon: 'fa-info-circle'
  },
  {
    ref: AppRoutes.impressum(),
    text: 'Impressum',
    appendLine: false,
    icon: 'fa-address-card'
  }
];
