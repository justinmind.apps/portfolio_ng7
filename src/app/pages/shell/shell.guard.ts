import { AuthService } from 'src/app/core/auth/auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShellGuard implements CanActivate {
  constructor(private _auth: AuthService, private _router: Router) {}

  async canActivate(): Promise<boolean> {
    const isLoggedIn = !!(await this._auth.getUser());

    if (!isLoggedIn) {
      this._router.navigate(['/signin']);
    }

    return isLoggedIn;
  }
}
