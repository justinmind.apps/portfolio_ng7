import {
  AfterViewInit,
  Component,
  HostBinding,
  OnDestroy,
  OnInit
  } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger
  } from '@angular/animations';
import { Emittable, Emitter } from '@ngxs-labs/emitter';
import { LayoutState } from 'src/app/core/layout/layout.state';
import { Observable, Subscription } from 'rxjs';
import { Select } from '@ngxs/store';
import { take } from 'rxjs/operators';

@Component({
  selector: 'shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  animations: [
    trigger('toggle', [
      state('hidden', style({ opacity: 0, transform: 'translateY(-100%)' })),
      state('visible', style({ opacity: 1, transform: 'translateY(0)' })),
      transition('* => *', animate('200ms ease-in'))
    ])
  ]
})
export class ShellComponent implements OnInit, AfterViewInit, OnDestroy {
  // *****************************
  // NGXS Store
  // *****************************
  @Select(LayoutState.isMediumUp) isMediumUp$: Observable<boolean>;

  @Select(LayoutState.sidenavIsVisible) sidenavIsVisible$: Observable<boolean>;

  @Emitter(LayoutState.stopLoading)
  private stopLoading: Emittable<void>;

  @Emitter(LayoutState.toggleSidenav)
  public toggleSidenav: Emittable<void>;

  @Emitter(LayoutState.openSidenav)
  public openSidenav: Emittable<boolean>;

  // *****************************
  // Custom Properties
  // *****************************
  mediaSub: Subscription;

  sidenavIsVisibleSub: Subscription;

  // *****************************
  // Angular Listener & Bindings
  // *****************************
  @HostBinding('class.isMediumUp')
  isMediumUp: boolean;

  @HostBinding('class.sidenavIsVisible')
  sidenavIsVisible: boolean;

  // *****************************
  // Hooks
  // *****************************
  ngAfterViewInit() {
    this.stopLoading.emit();
  }

  ngOnDestroy() {
    this.mediaSub.unsubscribe();
    this.sidenavIsVisibleSub.unsubscribe();
  }

  ngOnInit() {
    this.mediaSub = this.isMediumUp$.subscribe(isMediumUp => {
      this.isMediumUp = isMediumUp;
      this.sidenavIsVisible = isMediumUp;
    });

    this.sidenavIsVisibleSub = this.sidenavIsVisible$.subscribe(
      sidenavIsVisible => (this.sidenavIsVisible = sidenavIsVisible)
    );
  }
}
