import { Direction } from 'src/app/shared/models/direction.model';
import {
  Directive,
  ElementRef,
  OnDestroy,
  OnInit
  } from '@angular/core';
import {
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  share,
  tap,
  throttleTime
  } from 'rxjs/operators';
import { Emittable, Emitter } from '@ngxs-labs/emitter';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { LayoutState } from 'src/app/core/layout/layout.state';
import { nativeElement } from './nativeElement.model';
import { Select } from '@ngxs/store';

@Directive({
  selector: '[bkShell]'
})
export class ShellDirective implements OnInit, OnDestroy {
  @Emitter(LayoutState.showToolbar)
  private showToolbar: Emittable<boolean>;

  constructor(private el: ElementRef) {}

  scrollUp$: Subscription;
  scrollDown$: Subscription;

  ngOnInit() {
    // TODO: Maybe using renderer2 for non dom manipulations
    const scroll$ = fromEvent(this.el.nativeElement, 'scroll').pipe(
      throttleTime(10),
      map((scroll: nativeElement) => scroll.srcElement.scrollTop),
      pairwise(),
      map(([y1, y2]): Direction => (y2 < y1 ? Direction.Up : Direction.Down)),
      distinctUntilChanged(),
      share()
    );

    const scrollUp$ = scroll$.pipe(
      filter(direction => direction === Direction.Up)
    );

    const scrollDown = scroll$.pipe(
      filter(direction => direction === Direction.Down)
    );

    this.scrollUp$ = scrollUp$.subscribe(() => {
      console.warn('Scrolling up ...');
      this.showToolbar.emit(true);
    });
    this.scrollDown$ = scrollDown.subscribe(() => {
      console.warn('Scrolling up ...');
      this.showToolbar.emit(false);
    });
  }

  ngOnDestroy() {
    // this.scrollUp$.unsubscribe();
    // this.scrollDown$.unsubscribe();
  }
}
