import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShellComponent } from './shell.component';

const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview'
      },
      {
        path: 'overview',
        loadChildren: '../overview/overview.module#OverviewModule'
      },
      {
        path: 'skills',
        loadChildren: '../skills/skills.module#SkillsModule'
      },
      {
        path: 'milestones',
        loadChildren: '../milestones/milestones.module#MilestonesModule'
      },
      {
        path: 'about-portfolio',
        loadChildren:
          '../about-portfolio/about-portfolio.module#AboutPortfolioModule'
      },
      {
        path: '**',
        redirectTo: 'overview'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShellRoutingModule {}
