export interface nativeElement {
  srcElement: {
    scrollTop: number;
  };
}
