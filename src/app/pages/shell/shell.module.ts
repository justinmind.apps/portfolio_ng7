import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { ShellComponent } from './shell.component';
import { ShellDirective } from './shell.directive';
import { ShellRoutingModule } from './shell-routing.module';
import { SidenavComponent } from 'src/app/components/sidenav/sidenav.component';
import { ToolbarComponent } from 'src/app/components/toolbar/toolbar.component';
import { ToolbarDirective } from 'src/app/components/toolbar/toolbar.directive';

@NgModule({
  declarations: [
    ShellComponent,
    ToolbarComponent,
    SidenavComponent,
    ShellDirective,
    ToolbarDirective
  ],
  imports: [CommonModule, ShellRoutingModule, FlexLayoutModule]
})
export class ShellModule {}
