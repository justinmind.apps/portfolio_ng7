import { AfterViewInit, Component } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Emittable, Emitter } from '@ngxs-labs/emitter';
import { LayoutState } from 'src/app/core/layout/layout.state';
import { Router } from '@angular/router';

@Component({
  selector: 'signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements AfterViewInit {
  @Emitter(LayoutState.stopLoading)
  private stopLoading: Emittable<void>;

  constructor(private auth: AuthService, private router: Router) {}

  async signInWithGoogle() {
    const loggedIn = await this.auth.googleLogin();
    if (loggedIn) {
      this.router.navigate(['/']);
    }
  }

  ngAfterViewInit() {
    this.stopLoading.emit();
  }
}
