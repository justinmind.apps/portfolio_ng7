import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutPortfolioRoutingModule } from './about-portfolio-routing.module';
import { AboutPortfolioComponent } from './about-portfolio.component';

@NgModule({
  declarations: [AboutPortfolioComponent],
  imports: [
    CommonModule,
    AboutPortfolioRoutingModule
  ]
})
export class AboutPortfolioModule { }
