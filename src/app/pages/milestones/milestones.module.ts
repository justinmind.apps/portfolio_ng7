import { CommonModule } from '@angular/common';
import { MilestoneComponent } from './milestone/milestone.component';
import { MilestonesComponent } from './milestones.component';
import { MilestonesRoutingModule } from './milestones-routing.module';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [MilestonesComponent, MilestoneComponent],
  imports: [CommonModule, MilestonesRoutingModule]
})
export class MilestonesModule {}
