import { Milestone, milestoneType } from './milestone/milestone.model';

export const milestones: Milestone[] = [
  {
    start: new Date(2000, 11),
    end: new Date(2006, 11),
    type: milestoneType.School,
    institution: 'Karl Ernst Gymnasium',
    location: 'Amorbach, Germany',
    isImportant: false,
    description: 'School'
  },
  {
    start: new Date(2007, 0),
    end: new Date(2008, 5),
    type: milestoneType.School,
    institution: 'Tilemann Gymnasium',
    location: 'Limburg an der Lahn, Germany',
    isImportant: false,
    graduation: 'Middle Maturity',
    description: 'School'
  },
  {
    start: new Date(2008, 6),
    end: new Date(2008, 8),
    type: milestoneType['Further Education'],
    institution: 'Further Education BvB',
    location: 'Diez, Germany',
    isImportant: false,
    description: 'School'
  },
  {
    start: new Date(2008, 9),
    end: new Date(2008, 9),
    type: milestoneType['Further Education'],
    institution: 'Holter Aufzüge',
    location: 'Niederneisen, Germany',
    isImportant: false,
    description: 'Internship - Electrical engineer'
  },
  {
    start: new Date(2010, 9),
    end: new Date(20011, 9),
    type: milestoneType['Further Education'],
    institution: 'Schooling Nikolaus-August-Otto School',
    location: 'Diez, Germany',
    isImportant: false,
    description: 'School - Electrical engineer'
  },
  {
    start: new Date(2010, 9),
    end: new Date(20011, 9),
    type: milestoneType['Further Education'],
    institution: 'Holter Aufzüge',
    location: 'Diez, Germany',
    isImportant: false,
    description: 'Education - Electrical engineer'
  },
  {
    start: new Date(2010, 9),
    end: new Date(2011, 0),
    type: milestoneType.Misc,
    location: 'Hahnstätten, Germany',
    isImportant: false,
    description: 'Pausing'
  },
  {
    start: new Date(2011, 1),
    end: new Date(2011, 8),
    type: milestoneType['Further Education'],
    institution: 'Further Education Juwel',
    location: 'Diez, Germany',
    isImportant: false,
    description: 'School'
  },
  {
    start: new Date(2010, 7),
    end: new Date(2012, 5),
    type: milestoneType['Further Education'],
    institution: 'Distance Learning ILS',
    location: 'Hamburg, Germany',
    isImportant: false,
    graduation: 'Verified Webdesigner',
    description: 'School'
  },
  {
    start: new Date(2011, 9),
    end: new Date(2011, 10),
    type: milestoneType.Job,
    institution: 'Elringklinger GmbH',
    location: 'Runkel, Germany',
    isImportant: false,
    description: 'Assistant'
  },
  {
    start: new Date(2012, 1),
    end: new Date(2012, 2),
    type: milestoneType.School,
    institution: 'Further Education Trigon',
    location: 'Diez, Germany',
    isImportant: false,
    description: 'School'
  },
  {
    start: new Date(2012, 2),
    end: new Date(2012, 8),
    type: milestoneType.Misc,
    location: 'Hahnstätten, Germany',
    isImportant: false,
    description: 'Pausing'
  },
  {
    start: new Date(2012, 11),
    end: new Date(2015, 5),
    type: milestoneType.School,
    institution: 'Peter-Paul-Cahensly Gymnasium',
    location: 'Limburg an der Lahn, Germany',
    isImportant: false,
    graduation: 'High School',
    description: 'School'
  },
  {
    start: new Date(2012, 11),
    end: new Date(2015, 6),
    type: milestoneType.Job,
    institution: 'oh22systems GmbH',
    location: 'Bad Camberg, Germany',
    isImportant: true,
    graduation: 'IT specialist for application development',
    description: '.NET- & Database- & App-Developer'
  },
  {
    start: new Date(2012, 11),
    end: new Date(2015, 6),
    type: milestoneType.School,
    institution: 'Friedrich-Dessauer School',
    location: 'Limburg an der Lahn, Germany',
    isImportant: false,
    graduation: 'IT specialist for application development',
    description: 'School'
  },
  {
    start: new Date(2015, 7),
    end: new Date(2019, 1),
    type: milestoneType.University,
    institution: 'Physics, Goethe University',
    location: 'Frankfurt am Main, Germany',
    isImportant: true,
    description: 'Studies - Physics'
  },
  {
    start: new Date(2016, 1),
    end: new Date(2019, 2),
    type: milestoneType.Job,
    institution: 'Goethe University',
    location: 'Frankfurt am Main, Germany',
    isImportant: true,
    description: 'Teamcoordinator, Administrator, Project Manager, Developer'
  },
  {
    start: new Date(2016, 2),
    type: milestoneType.Job,
    location: 'Hahnstätten, Germany',
    isImportant: true,
    description: 'Developing Angular, Firestore, NGXS'
  }
];
