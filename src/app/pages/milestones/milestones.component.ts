import { Component, OnInit } from '@angular/core';
import { Milestone } from './milestone/milestone.model';
import { milestones } from './milestones';

@Component({
  selector: 'milestones',
  templateUrl: './milestones.component.html',
  styleUrls: ['./milestones.component.scss']
})
export class MilestonesComponent implements OnInit {
  milestones: Milestone[];
  constructor() {}

  ngOnInit() {
    console.warn([...milestones][0].start);
    this.milestones = [...milestones].reverse();
  }
}
