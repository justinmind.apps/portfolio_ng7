export interface Milestone {
  start: Date;
  end?: Date;
  type: MilestoneType;
  institution?: string;
  location: string;
  isImportant: boolean;
  description: string;
  graduation?: string;
}

export type MilestoneType =
  | 'School'
  | 'Further Education'
  | 'University'
  | 'Job'
  | 'Misc';

export enum milestoneType {
  School = 'School',
  'Further Education' = 'Further Education',
  University = 'University',
  Job = 'Job',
  Misc = 'Misc'
}
