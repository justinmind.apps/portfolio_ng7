import { Component, Input, OnInit } from '@angular/core';
import { Milestone } from './milestone.model';

@Component({
  selector: 'milestone',
  templateUrl: './milestone.component.html',
  styleUrls: ['./milestone.component.scss']
})
export class MilestoneComponent {
  @Input()
  milestone: Milestone;
}
