import {
  animate,
  style,
  transition,
  trigger
  } from '@angular/animations';

export const skillAnimations = [
  trigger('skillStartAnimation', [
    transition(':enter', [
      style({ width: '0%' }),
      animate('.4s', style({ width: '*' }))
    ])
  ])
];
