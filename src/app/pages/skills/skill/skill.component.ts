import { Component, Input } from '@angular/core';
import { Skill } from './skill.model';
import { skillAnimations } from './skill.animations';

@Component({
  selector: '[skill]',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss'],
  animations: [skillAnimations]
})
export class SkillComponent {
  @Input()
  skill: Skill;

  animate = false;

  get skillLevel() {
    return 'skill-' + this.skill.level;
  }

  get skillColor() {
    const level = this.skill.level;
    if (level >= 50) {
      return 'green-A200';
    } else if (level >= 1) {
      return 'green-A100';
    } else {
      return '';
    }
  }
}
