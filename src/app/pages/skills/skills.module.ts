import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SkillComponent } from './skill/skill.component';
import { SkillsComponent } from './skills.component';
import { SkillsRoutingModule } from './skills-routing.module';

@NgModule({
  declarations: [SkillsComponent, SkillComponent],
  imports: [CommonModule, SkillsRoutingModule]
})
export class SkillsModule {}
