import { Component, OnInit } from '@angular/core';
import { nextContext } from '@angular/core/src/render3';
import { Skill } from './skill/skill.model';
import { skills } from './skills';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent {
  skills = skills.sort((prev, next) => this.compareSkillLevel(prev, next));

  compareSkillLevel(prev: Skill, next: Skill) {
    return prev.level > next.level ? -1 : prev.level < next.level ? 1 : 0;
  }
}
